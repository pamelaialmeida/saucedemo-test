class LoginPage {

    #usernameElement = '[data-test="username"]';
    #passwordElement = '[data-test="password"]';
    #loginButtonElement = '[data-test="login-button"]';

    accessSaucedemoLoginPage(){
        cy.visit('/')
    }

    loginIntoSaucedemo(username, password){
        cy.get(this.#usernameElement).type(username)
        cy.get(this.#passwordElement).type(password)
        cy.get(this.#loginButtonElement).click()
    }

}

export default new LoginPage()