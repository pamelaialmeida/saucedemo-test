class InventoryPage{

    #itemNameElement = '.inventory_item_name';
    #sortingOptionsElement = '.product_sort_container';

    getItemsList(){
        return cy.get('.inventory_item_name').then($items => {
            const inventoryItems = Array.from($items)
            return inventoryItems.map((item) => item.innerText)
        })
    }

    selectSortingOption(option){
        cy.get(this.#sortingOptionsElement).select(option)
    }

}

export default new InventoryPage()