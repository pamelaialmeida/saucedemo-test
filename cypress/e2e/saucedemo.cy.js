/// <reference types="cypress" />
import userLoginData from "../fixtures/standard_user.json"
import loginPage from "../support/page-objects/login.page"
import inventoryPage from "../support/page-objects/inventory.page"

describe('Saucedemo Testing', () => {

  it('Should change the alphabetical name sorting from A -> Z to Z -> A ', () => {
    // Go to https://www.saucedemo.com/
    loginPage.accessSaucedemoLoginPage()

    // Log in to the site.
    loginPage.loginIntoSaucedemo(userLoginData.username, userLoginData.password)

    
    // Verify that the items are sorted by Name ( A -> Z ).
    inventoryPage.getItemsList().then(items => {
      expect(items).to.be.sorted()
    })

    // Change the sorting to Name ( Z -> A).
    inventoryPage.selectSortingOption('Name (Z to A)')

    // Verify that the items are sorted by Name ( Z -> A ).
    inventoryPage.getItemsList().then(items => {
      expect(items).to.be.sorted({descending: true})
    })

  })

})